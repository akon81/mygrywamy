<?php

namespace App\Providers;

use App\ViewComposers\HomeComposer;
use App\ViewComposers\TagsComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App\ViewComposers\PublisherComposer;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // View::composer('home', TagsComposer::class);
        View::composer('home', TagsComposer::class);
        View::composer('home', PublisherComposer::class);
        View::composer('home', HomeComposer::class);

        View::composer('contact', TagsComposer::class);
        View::composer('contact', PublisherComposer::class);

        View::composer('about_us', TagsComposer::class);
        View::composer('about_us', PublisherComposer::class);

        View::composer('reviews', TagsComposer::class);
        View::composer('reviews', PublisherComposer::class);

        View::composer('tag', TagsComposer::class);
        View::composer('tag', PublisherComposer::class);

        View::composer('publishing', TagsComposer::class);
        View::composer('publishing', PublisherComposer::class);

        View::composer('review', TagsComposer::class);
        View::composer('review', PublisherComposer::class);

        View::composer('event', TagsComposer::class);
        View::composer('event', PublisherComposer::class);

        View::composer('announcement', TagsComposer::class);
        View::composer('announcement', PublisherComposer::class);

        View::composer('search', TagsComposer::class);
        View::composer('search', PublisherComposer::class);
    }
}
