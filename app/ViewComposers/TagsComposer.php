<?php

namespace App\ViewComposers;

use App\Models\Tag;
use Illuminate\View\View;

class TagsComposer
{
    /**
     * Bind data to the view.
     */
    public function compose(View $view): void
    {
        $tags = cache()->remember(
            'tags',
            3600,
            fn () => Tag::all()
        );

        $view->with('tags', $tags);
    }
}
