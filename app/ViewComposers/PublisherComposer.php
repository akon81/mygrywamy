<?php

namespace App\ViewComposers;

use App\Models\Publisher;
use Illuminate\View\View;

class PublisherComposer
{
    /**
     * Bind data to the view.
     */
    public function compose(View $view): void
    {
        $publishings = cache()->remember(
            'publishings',
            3600,
            fn () => Publisher::all()
        );

        $view->with('publishings', $publishings);
    }
}
