<?php

namespace App\ViewComposers;

use App\Models\Tag;
use App\Models\Post;
use Illuminate\View\View;

class HomeComposer
{
    /**
     * Bind data to the view.
     */
    public function compose(View $view): void
    {
        $slider = Post::published()->slider()->limit(10)->get();
        $last_added = Post::published()->limit(6)->get();
                
        $view->with(compact(['slider', 'last_added']));
    }
}
