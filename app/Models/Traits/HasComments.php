<?php

namespace App\Models\Traits;

use App\Models\Comment;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait HasComments
{
    /**
     * Return all comments for this model.
     */
    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * Attach a comment to this model as a specific user.
     */
    public function comment(?User $user, string $comment, ?string $signature, ?bool $is_approved, ?string $email): Model
    {
        $comment = new Comment([
            'comment' => $comment,
            'is_approved' => $is_approved,
            'user_id' => is_null($user) ? null : $user->getKey(),
            'post_id' => $this->getKey(),
            'signature' => is_null($signature) ? null : $signature,
            'email' => is_null($email) ? null : $email,
        ]);

        return $this->comments()->save($comment);
    }
}
