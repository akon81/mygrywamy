<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug',
        'content',
        'in_menu_top',
        'in_menu_bottom',
        'is_active',
        'order',
        'meta_title',
        'meta_description',
    ];

    protected $casts = [
        'in_menu_top' => 'boolean',
        'in_menu_bottom' => 'boolean',
        'is_active' => 'boolean',
    ];
}
