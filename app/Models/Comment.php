<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = [
        'comment',
        'signature',
        'email',
        'is_approved',
        'user_id',
        'post_id',
    ];

    protected $casts = [
        'is_approved' => 'boolean',
    ];

    public function getFormattedDate()
    {
        return $this->created_at->format('Y-m-d H:i:s');
    }

    public function scopeApproved($query)
    {
        return $query->where('is_approved', true);
    }

    public function approve()
    {
        $this->update([
            'is_approved' => true,
        ]);

        return $this;
    }

    public function disapprove()
    {
        $this->update([
            'is_approved' => false,
        ]);

        return $this;
    }

    public function post(): BelongsTo
    {
        return $this->belongsTo(Post::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
