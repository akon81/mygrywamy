<?php

namespace App\Models;

use App\Models\Traits\HasComments;
use CyrildeWit\EloquentViewable\Contracts\Viewable;
use CyrildeWit\EloquentViewable\InteractsWithViews;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Post extends Model implements Viewable, HasMedia
{
    use HasFactory, InteractsWithViews, InteractsWithMedia, HasComments;

    protected $fillable = [
        'title',
        'slug',
        'teaser',
        'content',
        'thumbnail',
        'animation',
        'spirit',
        'user_id',
        'publisher_id',
        'is_active',
        'in_slider',
        'published_at',
        'age',
        'players',
        'time',
        'rating',
        'meta_title',
        'meta_description',
    ];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->crop('crop-center', 450, 350)
            ->queued();
        //->nonQueued();
    }

    protected $casts = [
        'published_at' => 'datetime',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function publisher(): BelongsTo
    {
        return $this->belongsTo(Publisher::class);
    }

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class);
    }

    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class);
    }

    public function artists(): BelongsToMany
    {
        return $this->belongsToMany(Artist::class);
    }

    public function authors(): BelongsToMany
    {
        return $this->belongsToMany(Author::class);
    }

    public function shortBody($words = 30): string
    {
        return Str::words(strip_tags($this->body), $words);
    }

    public function getFormattedDate()
    {
        return $this->published_at->format('Y-m-d');
    }

    public function getShortTeaser($limit = 30)
    {
        return Str::words($this->teaser, $limit);
    }

    public function getCategoryMainSlug()
    {
        return $this->categories->first()->slug;
    }

    public function getCategoryMainName()
    {
        return $this->categories->first()->name;
    }

    public function Publish(): bool
    {
        if($this->is_active and $this->published_at<now()) return true; else return false;
    }

    /**
     * Scope a query to only include active users.
     */
    public function scopePublished(Builder $query): void
    {
        $query->where('is_active', 1)->where('published_at', '<', now())->orderBy('published_at', 'desc');
    }

    /**
     * Scope a query to only include active users.
     */
    public function scopeSlider(Builder $query): void
    {
        $query->where('in_slider', 1);
    }

    public function getThumbnail()
    {
        if (str_starts_with($this->thumbnail, 'http')) {
            return $this->thumbnail;
        }

        return '/storage/'.$this->thumbnail;
    }

    public function humanReadTime(): int
    {
        $words = Str::wordCount(strip_tags($this->content));
        $minutes = ceil($words / 200);

        return $minutes;
    }
}
