<?php

namespace App\Console;

//use App\Actions\GenerateSitemap;

use App\Console\Commands\GenerateSitemap;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    protected $commands= [
        GenerateSitemap::class
    ];
    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule): void
    {
        $schedule->exec('php artisan queue:restart')->everyFiveMinutes();
        $schedule->exec('php artisan queue:work')->everyFiveMinutes();

        $schedule->command('generate:sitemap')->daily();
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
