<?php

namespace App\Console\Commands;

use App\Models\Post;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;
use Illuminate\Console\Command;

class GenerateSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate sitemap.xml';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $sitemap=Sitemap::create()
        ->add(Url::create('/home'))
        ->add(Url::create('/strona/o-nas'))
        ->add(Url::create('/kontakt'));

        Post::published()->each(function(Post $post) use ($sitemap) {
            $sitemap->add(Url::create('/'.$post->getCategoryMainSlug().'/'.$post->slug));
        });

        $sitemap->writeToFile(public_path('sitemap.xml'));
        $this->info('Sitemap generated successfully!');
    }
}