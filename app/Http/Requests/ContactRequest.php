<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'email' => 'required|string|email|min:5|max:255',
            'content' => 'required|string|min:3|max:1000',
        ];
    }

    public function messages(): array
    {
        return [
            'content.max' => 'Zbyt długa wiadomość. Przekroczyłeś limit znaków wiadomości',
            'content.min' => 'Zbyt krótka wiadomość. Napisz coś więcej :)',
        ];
    }
}
