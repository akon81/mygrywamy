<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'signature' => 'required|string|max:255',
            'comment' => 'required|string|min:3|max:1000',
        ];
    }

    public function messages(): array
    {
        return [
            'comment.max' => 'Zbyt długa wiadomość. Przekroczyłeś limit znaków wiadomości',
            'comment.min' => 'Zbyt krótka wiadomość. Napisz coś więcej :)',
        ];
    }
}
