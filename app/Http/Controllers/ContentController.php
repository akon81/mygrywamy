<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\Post;
use App\Models\Category;
use App\Models\Publisher;
use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CommentRequest;
use App\Notifications\CommentNotification;
use Illuminate\Support\Facades\Notification;

class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Vie
     */
    public function reviews_index(): View
    {
        $reviews = Category::where('slug', 'recenzja')->first()->posts()->published()->SimplePaginate(9);

        return view('reviews', ['reviews' => $reviews, 'kind' => 'Recenzje']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\View
     */
    public function events_index(): View
    {
        $reviews = Category::where('slug', 'wydarzenie')->first()->posts()->published()->SimplePaginate(9);

        return view('reviews', ['reviews' => $reviews, 'kind' => 'Wydarzenia']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\View
     */
    public function announcements_index(): View
    {
        $reviews = Category::where('slug', 'zapowiedz')->first()->posts()->published()->SimplePaginate(9);

        return view('reviews', ['reviews' => $reviews, 'kind' => 'Zapowiedzi']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\View
     */
    public function tag_index(Tag $tag): View
    {
        $posts = Tag::find($tag->id)->posts()->published()->SimplePaginate(9);

        return view('tag', compact(['posts', 'tag']));
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\View
     */
    public function publisher_index(Publisher $publisher): View
    {
        $posts = Publisher::find($publisher->id)->posts()->published()->SimplePaginate(9);

        return view('publishing', compact(['posts', 'publisher']));
    }

    /**
     * Display a review details
     *
     * @return \Illuminate\Http\View
     */
    public function show_review(Post $post): mixed
    {
        $categ = $post->categories()->first();
        if ($post->categories()->first()->slug == 'recenzja') {
            $view = 'review';
        }
        if ($post->categories()->first()->slug == 'zapowiedz') {
            $view = 'announcement';
        }
        if ($post->categories()->first()->slug == 'wydarzenie') {
            $view = 'event';
        }
        $comments = $post->comments()->approved()->SimplePaginate(10);

        if($post->publish() or (Auth::user() and Auth::user()->isAdmin)) {
            views($post)->cooldown(2)->record(); //dodanie naliczania wyswietlenia dla postu
            return view($view, compact(['post', 'comments']));
        } else return redirect('404');

        
    }

    public function search(Request $request)
    {
        $search = $request->input('search');

        // do zrobienia: zapisz do bazy frazę wyszukiwania

        $posts = Post::query()
            ->where('title', 'LIKE', "%{$search}%")
            ->orWhere('teaser', 'LIKE', "%{$search}%")
            ->orWhere('content', 'LIKE', "%{$search}%")
            ->published()->SimplePaginate(9);

        if ($posts->count() == 1) {
            $post = $posts->first();
            $categ = $post->categories()->first();
            if ($post->categories()->first()->slug == 'recenzja') {
                $view = 'review';
            }
            if ($post->categories()->first()->slug == 'wydarzenie') {
                $view = 'event';
            }
            views($post)->cooldown(2)->record();
            $comments = $post->comments()->approved()->SimplePaginate(10);

            return view($view, compact(['post', 'comments']));
        } else {
            return view('search', compact(['posts']));
        }
    }

    /**
     * Send contact message.
     *
     * @return \Illuminate\Http\Response
     */
    public function comment_send(CommentRequest $request, Post $post)
    {
        $request->validated();

        $admin_email = config('app.admin_email');
        $signature = $request->signature;
        $comment = $request->comment;
        $is_approved = false;

        if (Auth::user() and Auth::user()->isAdmin == true) {
            $is_approved = true;
        }

        $post->comment(is_null(Auth::user()) ? null : Auth::user(), $comment, $signature, $is_approved, null);

        if ($is_approved == false) {
            Notification::route('mail', $admin_email)->notify(new CommentNotification($signature, $comment, $post->title));
        }

        return redirect()->back()->with('status', 'Dziękujemy za dodanie komentarza. Po zaakceptowaniu zostanie opublikowany.');
    }
}
