<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use App\Models\Page;
use App\Models\Post;
use App\Notifications\ContactMessageNotification;
use Illuminate\Support\Facades\Notification;

class HomeController extends Controller
{
    /**
     * Show the application main page
     */
    public function index(): \Illuminate\Contracts\Support\Renderable
    {
        $slider = Post::published()->slider()->limit(10)->get();
        $last_added = Post::published()->limit(6)->get();

        return view('home', ['last_added' => $last_added, 'slider' => $slider]);
    }

    /**
     * Show the application contact page
     */
    public function contact(): \Illuminate\Contracts\Support\Renderable
    {
        return view('contact');
    }

    /**
     * Send contact message.
     *
     * @return \Illuminate\Http\Response
     */
    public function send(ContactRequest $request)
    {
        $request->validated();

        $admin_email = config('app.admin_email');
        $email = $request->email;
        $content = $request->content;

        Notification::route('mail', $admin_email)->notify(new ContactMessageNotification($email, $content));

        return redirect(route('contact'))->with('status', 'Wiadomość została wysłana. Dziękujemy');
    }

    /**
     * Show the application contact page
     */
    public function about(): \Illuminate\Contracts\Support\Renderable
    {
        $page = Page::find(1);

        return view('about_us', compact(['page']));
    }
}
