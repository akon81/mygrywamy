<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Actions\GenerateSitemap;

class SitemapController extends Controller
{
    public function makeSitemap(): void
    {        
        app(GenerateSitemap::class)->generate();    
    }

}