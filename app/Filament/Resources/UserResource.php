<?php

namespace App\Filament\Resources;

use Filament\Forms;
use App\Models\User;
use Filament\Tables;
use Filament\Resources\Form;
use Filament\Resources\Table;
use Filament\Resources\Resource;
use Illuminate\Support\Facades\Hash;
use Filament\Forms\Components\Toggle;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\TextInput;
use Filament\Tables\Columns\ToggleColumn;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Validation\Rules\Password;
use App\Filament\Resources\UserResource\Pages;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use App\Filament\Resources\UserResource\Pages\EditUser;
use App\Filament\Resources\UserResource\Pages\CreateUser;
use App\Filament\Resources\UserResource\RelationManagers;

class UserResource extends Resource
{
    protected static ?string $model = User::class;

    protected static ?string $navigationIcon = 'heroicon-o-users';

    protected static ?string $modelLabel = 'Użytkownika';

    protected static ?string $pluralModelLabel = 'użytkownicy';

    protected static ?string $navigationLabel = 'Użytkownicy';

    protected static ?int $navigationSort = 6;

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make('Profil użytkownika')->schema([
                    TextInput::make('name')->label('Nazwisko')->required(),
                    TextInput::make('email')->label('Email')->required()->unique(ignoreRecord: true),
                    TextInput::make('password')
                    ->label('Hasło')
                    ->required()
                    ->password()
                    ->dehydrateStateUsing(fn ($state) => Hash::make($state))
                    ->visible(fn ($livewire) => $livewire instanceof CreateUser)
                    ->rule(Password::default()),
                    Toggle::make('isAdmin')->label('Administrator'),
                    ]),
                Section::make('Nowe hasło użytkownika')->schema([
                    TextInput::make('new_password')
                    ->label('Nowe hasło')
                    ->nullable()
                    ->password()
                    ->rule(Password::default()),
                    TextInput::make('new_password_confirmation')
                    ->label('Powtórz nowe hasło')
                    ->password()
                    ->same('new_password')
                    ->requiredWith('new_password')                   
                    ])->visible(fn ($livewire) => $livewire instanceof EditUser),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('name')->label('Nazwisko'),
                Tables\Columns\TextColumn::make('email')->label('Email'),
                ToggleColumn::make('isAdmin')->label('Administrator'),
                Tables\Columns\TextColumn::make('created_at')->label('Profil utworzono')
                ->dateTime('Y-m-d')->size('sm'),
                Tables\Columns\TextColumn::make('updated_at')->label('Edytowano')
                ->dateTime('Y-m-d')->size('sm'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }
    
    public static function getRelations(): array
    {
        return [
            //
        ];
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListUsers::route('/'),
            'create' => Pages\CreateUser::route('/create'),
            'edit' => Pages\EditUser::route('/{record}/edit'),
        ];
    }    
}
