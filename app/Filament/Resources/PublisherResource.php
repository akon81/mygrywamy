<?php

namespace App\Filament\Resources;

use Closure;
use Filament\Tables;
use App\Models\Publisher;
use Illuminate\Support\Str;
use Filament\Resources\Form;
use Filament\Resources\Table;
use Filament\Resources\Resource;
use Filament\Forms\Components\Grid;
use Illuminate\Support\Facades\Cache;
use Filament\Tables\Columns\TextColumn;
use Filament\Forms\Components\TextInput;
use Filament\Tables\Columns\ImageColumn;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\RichEditor;
use Filament\Tables\Actions\DeleteAction;
use App\Filament\Resources\PublisherResource\Pages;

class PublisherResource extends Resource
{
    protected static ?string $model = Publisher::class;

    protected static ?string $modelLabel = 'wydawnictwo';

    protected static ?string $pluralModelLabel = 'wydawnictwa';

    protected static ?string $navigationLabel = 'Wydawnictwa';

    protected static ?string $navigationIcon = 'heroicon-o-identification';

    protected static ?int $navigationSort = 4;

    protected static ?string $navigationGroup = 'Dane';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Grid::make(1)
                    ->schema([
                        TextInput::make('name')->label('Wydawnictwo')
                            ->reactive()
                            ->afterStateUpdated(function (Closure $set, $state) {
                                $set('slug', Str::slug($state));
                            })
                            ->autofocus()->unique(ignoreRecord: true)->required()->maxLength(255),
                        TextInput::make('slug')->label('Slug')->disabled()->required(),
                        TextInput::make('www')->label('WWW')
                            ->maxLength(255),
                        FileUpload::make('logo')->image()->directory('publisher')->preserveFilenames(),
                        RichEditor::make('description')::make('description')->label('Opis')
                            ->maxLength(65535)->fileAttachmentsDirectory('attachments')->toolbarButtons([
                            'attachFiles',
                            'bold',
                            'h2',
                            'h3',
                            'italic',
                            'link',
                            ]),
                    ]),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('name')->label('Wydawnictwo')->sortable()->searchable(),
                ImageColumn::make('logo'),
                TextColumn::make('www')->label('WWW'),
                TextColumn::make('created_at')->label('Utworzono')
                    ->date('Y-m-d'),
                TextColumn::make('updated_at')->label('Edytowano')
                    ->date('Y-m-d'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                DeleteAction::make()->after(function () {
                    Cache::flush();
                }),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make()->after(function () {
                    Cache::flush();
                }),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListPublishers::route('/'),
            'create' => Pages\CreatePublisher::route('/create'),
            'edit' => Pages\EditPublisher::route('/{record}/edit'),
        ];
    }
}
