<?php

namespace App\Filament\Resources;

use Closure;
use App\Models\Post;
use Illuminate\Support\Str;
use Filament\Resources\Form;
use Filament\Resources\Table;
use Filament\Resources\Resource;
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Toggle;
use Illuminate\Support\Facades\Cache;
use FilamentTiptapEditor\TiptapEditor;
use Filament\Forms\Components\Fieldset;
use Filament\Forms\Components\Textarea;
use Filament\Tables\Actions\EditAction;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Columns\ViewColumn;
use Filament\Forms\Components\TextInput;
use Filament\Tables\Columns\BadgeColumn;
use Filament\Tables\Columns\ImageColumn;
use Filament\Forms\Components\FileUpload;
use Filament\Tables\Actions\DeleteAction;
use Filament\Tables\Columns\ToggleColumn;
use Filament\Tables\Filters\SelectFilter;
use Filament\Tables\Filters\TernaryFilter;
use Filament\Forms\Components\DateTimePicker;
use Filament\Tables\Actions\DeleteBulkAction;
use Nuhel\FilamentCroppie\Components\Croppie;
use App\Filament\Resources\PostResource\Pages;
use Filament\Forms\Components\SpatieMediaLibraryFileUpload;

class PostResource extends Resource
{
    protected static ?string $model = Post::class;

    protected static ?string $modelLabel = 'Post';

    protected static ?string $pluralModelLabel = 'posty';

    protected static ?string $navigationLabel = 'Posty';

    protected static ?string $navigationIcon = 'heroicon-o-clipboard-list';

    protected static ?int $navigationSort = 2;

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Grid::make(2)
                    ->schema([
                        TextInput::make('title')->label('Tytuł')->reactive()
                            ->afterStateUpdated(function (Closure $set, $state) {
                                $set('slug', Str::slug($state));
                                $set('meta_title', $state);
                            })
                            ->autofocus()->unique(ignoreRecord: true)->required(),
                        TextInput::make('slug')->label('Slug')->disabled()->required(),
                        Textarea::make('teaser')->label('Zajawka')->columnSpan(['2xl' => 2])
                            ->maxLength(65535),
                        TiptapEditor::make('content')->label('Treść wpisu')->columnSpan(['2xl' => 2]),
                        Croppie::make('thumbnail')->directory('thumbnail')->label('Miniaturka')->columnSpan(['2xl' => 2])
                            ->modalHeading('Przytnij obrazek')
                            ->imageResizeTargetWidth('400')
                            ->imageResizeTargetHeight('400')
                        //->keepOriginalSize()
                            ->modalSize('xl')
                            ->enableOpen()
                            ->preserveFilenames(),
                        FileUpload::make('animation')->directory('animations')->label('Plik js z animacją')->preserveFilenames()->helperText('Plik z animacją GSAP, jeśli nie ma defaulowa animacja'),
                        FileUpload::make('spirit')->directory('spirits')->label('Grafika do animacji')->preserveFilenames(),
                        Select::make('category_id')->label('Rodzaj posta')
                            ->relationship('categories', 'name')
                            ->multiple()
                            ->preload()
                            ->required()->columnSpan(['2xl' => 2]),
                        Select::make('publisher_id')->label('Wydawnictwo')
                            ->relationship('publisher', 'name')
                            ->createOptionForm([
                              TextInput::make('name')->label('Wydawnictwo')->required()->reactive()
                                  ->afterStateUpdated(function (Closure $set, $state) {
                                      $set('slug', Str::slug($state));
                                  }),
                              TextInput::make('slug')->label('Slug')->disabled()->required(),
                            ])->after(function () {
                                Cache::flush();
                            }),
                        Select::make('user_id')->label('Twórca posta')
                            ->relationship('user', 'name')
                            ->required()->default(1),
                        Select::make('author_id')->label('Autorzy')
                            ->relationship('authors', 'name')
                            ->preload()
                            ->multiple()
                            ->createOptionForm([
                                TextInput::make('name')->label('Autor')->required(),
                            ]),
                        Select::make('artist_id')->label('Ilustratorzy')
                            ->relationship('artists', 'name')
                            ->preload()
                            ->multiple()
                            ->createOptionForm([
                                TextInput::make('name')->label('Ilustrator')->required(),
                            ]),
                        Select::make('tag_id')->label('Tagi')
                            ->relationship('tags', 'tag')
                            ->preload()
                            ->multiple()
                            ->columnSpan(['2xl' => 2])
                            ->createOptionForm([
                                TextInput::make('tag')->required()->reactive()
                                    ->afterStateUpdated(function (Closure $set, $state) {
                                        $set('slug', Str::slug($state));
                                    }),
                                TextInput::make('slug')->label('Slug')->disabled()->required(),
                                Textarea::make('description')->label('Opis'),
                            ])->after(function () {
                                Cache::flush();
                            }),
                        TextInput::make('meta_title')
                            ->maxLength(255),
                        TextInput::make('meta_description')
                            ->maxLength(255),
                        DateTimePicker::make('published_at')->format('Y-m-d h:i:s')->label('Opublikowany')->default(now())->columnSpan(['2xl' => 2]),
                        Toggle::make('is_active')->label('Aktywny')
                            ->columnSpan(['2xl' => 2]),
                        Toggle::make('in_slider')->label('W sliderze')
                            ->columnSpan(['2xl' => 2]),
                        Fieldset::make('Parametry')
                            ->schema(static::getParametersSchema()),
                        SpatieMediaLibraryFileUpload::make('gallery')->label('Galeria')->columnSpan(['2xl' => 2])
                            ->collection('gallery')
                            ->conversion('thumb')
                            ->multiple()
                            ->enableReordering()
                            ->preserveFilenames(),
                    ]),
            ]);
    }

    public static function getParametersSchema(): array
    {
        $fieldsArray = [];

        $fieldsArray = array_merge([
            TextInput::make('players')->label('Liczba graczy')
                ->maxLength(255),
            TextInput::make('age')->label('Wiek')
                ->numeric(),
            TextInput::make('time')->label('Czas gry')
                ->maxLength(255),
            TextInput::make('rating')->label('Ocena')
                ->maxLength(255),
        ], $fieldsArray);

        return $fieldsArray;
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('title')->label('Tytuł')->sortable()->searchable(),
                BadgeColumn::make('categories.name')
                    ->color(static function ($state): string {
                        if ($state === 'Recenzja') {
                            return 'success';
                        }
                        if ($state === 'Zapowiedź') {
                            return 'secondary';
                        }
                        if ($state === 'Wydarzenie') {
                            return 'warning';
                        }

                        return 'danger';
                    })
                    ->label('Rodzaj')->sortable(),
                ImageColumn::make('thumbnail')->label('Miniaturka'),
                TextColumn::make('teaser')->label('Zajawka')->size('sm')->words(5)->wrap()->searchable(),
                ToggleColumn::make('is_active')->label('Aktywna'),
                ToggleColumn::make('in_slider')->label('W sliderze'),
                TextColumn::make('rating')->label('Ocena')->sortable(),
                ViewColumn::make('views')->label('Wyświetlenia')->view('vendor.filament.tables.count-views')->sortable(),
                TextColumn::make('comments_count')->counts('comments')->label('Komentarze')->sortable(),
                TextColumn::make('published_at')->label('Opublikowany')->dateTime('Y-m-d')->size('sm'),
                TextColumn::make('updated_at')->label('Edytowany')
                    ->dateTime('Y-m-d')->size('sm'),
            ])
            ->filters([
                TernaryFilter::make('is_active')->label('Aktywny'),
                SelectFilter::make('category_id')->label('Rodzaj')->attribute('categories.name'),
            ])
            ->actions([
                EditAction::make(),
                DeleteAction::make(),
            ])
            ->bulkActions([
                DeleteBulkAction::make(),
            ])->defaultSort('published_at', 'desc');
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListPosts::route('/'),
            'create' => Pages\CreatePost::route('/create'),
            'edit' => Pages\EditPost::route('/{record}/edit'),
        ];
    }
}
