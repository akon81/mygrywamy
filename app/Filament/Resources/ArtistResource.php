<?php

namespace App\Filament\Resources;

use App\Filament\Resources\ArtistResource\Pages;
use App\Models\Artist;
use Filament\Forms\Components\Card;
use Filament\Forms\Components\RichEditor;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;

class ArtistResource extends Resource
{
    protected static ?string $model = Artist::class;

    protected static ?string $modelLabel = 'ilustratora';

    protected static ?string $pluralModelLabel = 'ilustratorzy';

    protected static ?string $navigationLabel = 'Ilustratorzy';

    protected static ?string $navigationIcon = 'heroicon-o-users';

    protected static ?int $navigationSort = 3;

    protected static ?string $navigationGroup = 'Dane';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Card::make()
                    ->schema([
                        TextInput::make('name')->label('Imię i Nazwisko')->required(),
                        RichEditor::make('description')->label('Opis'),
                    ]),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('id')->sortable(),
                TextColumn::make('name')->label('Imię i Nazwisko')->size('lg')->sortable()->searchable(),
                TextColumn::make('description')->html()->size('sm')->label('Opis')->sortable()->searchable(),
                TextColumn::make('created_at')->label('Utworzono')->date('Y-m-d'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ManageArtists::route('/'),
        ];
    }
}
