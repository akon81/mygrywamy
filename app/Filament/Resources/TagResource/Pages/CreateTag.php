<?php

namespace App\Filament\Resources\TagResource\Pages;

use Illuminate\Support\Facades\Cache;
use Filament\Resources\Pages\CreateRecord;
use App\Filament\Resources\TagResource;

class CreateTag extends CreateRecord
{
    protected static string $resource = TagResource::class;

    protected function afterCreate(): void
    {
        Cache::flush();
    }
    
}
