<?php

namespace App\Filament\Resources;

use App\Filament\Resources\PageResource\Pages;
use App\Models\Page;
use Closure;
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Toggle;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\ToggleColumn;
use FilamentTiptapEditor\TiptapEditor;
use Illuminate\Support\Str;

class PageResource extends Resource
{
    protected static ?string $model = Page::class;

    protected static ?string $modelLabel = 'Stronę';

    protected static ?string $pluralModelLabel = 'strony';

    protected static ?string $navigationLabel = 'Strony';

    protected static ?string $navigationIcon = 'heroicon-o-template';

    protected static ?int $navigationSort = 5;

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Grid::make(2)
                    ->schema([
                        TextInput::make('title')->label('Tytuł')->reactive()
                            ->afterStateUpdated(function (Closure $set, $state) {
                                $set('slug', Str::slug($state));
                                $set('meta_title', $state);
                            })
                            ->autofocus()->unique(ignoreRecord: true)->required(),
                        TextInput::make('slug')->label('Slug')->disabled()->required(),
                        TiptapEditor::make('content')->label('Treść')->columnSpan(['2xl' => 2]),
                        Toggle::make('in_menu_top')->label('W menu górnym')
                            ->columnSpan(['2xl' => 2]),
                        Toggle::make('in_menu_bottom')->label('W menu dolnym')
                            ->columnSpan(['2xl' => 2]),
                        Toggle::make('is_active')->label('Aktywny'),
                        TextInput::make('order')->label('Pozycja')->numeric()->required()->columnSpan(['2xl' => 2]),
                        TextInput::make('meta_title')
                            ->maxLength(255),
                        TextInput::make('meta_description')
                            ->maxLength(255),
                    ]),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('title')->label('Tytuł'),
                Tables\Columns\TextColumn::make('slug')->label('Slug'),
                ToggleColumn::make('in_menu_top')->label('W menu górnym'),
                ToggleColumn::make('in_menu_bottom')->label('W menu dolnym'),
                ToggleColumn::make('is_active')->label('Aktywna'),
                Tables\Columns\TextColumn::make('order')->label('Pozycja'),
                Tables\Columns\TextColumn::make('created_at')->label('Utworzono')
                    ->dateTime('Y-m-d')->size('sm'),
                Tables\Columns\TextColumn::make('updated_at')->label('Edytowano')
                    ->dateTime('Y-m-d')->size('sm'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ])->defaultSort('order', 'asc');
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListPages::route('/'),
            'create' => Pages\CreatePage::route('/create'),
            'edit' => Pages\EditPage::route('/{record}/edit'),
        ];
    }
}
