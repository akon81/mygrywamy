<?php

namespace App\Filament\Resources;

use Filament\Tables;
use App\Models\Comment;
use Filament\Resources\Form;
use Filament\Resources\Table;
use Filament\Resources\Resource;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Toggle;
use Filament\Forms\Components\Textarea;
use Filament\Tables\Columns\TextColumn;
use Filament\Forms\Components\TextInput;
use Filament\Tables\Columns\ToggleColumn;
use Filament\Tables\Filters\TernaryFilter;
use App\Filament\Resources\CommentResource\Pages;

class CommentResource extends Resource
{
    protected static ?string $model = Comment::class;

    protected static ?string $modelLabel = 'Komentarz';

    protected static ?string $pluralModelLabel = 'komentarze';

    protected static ?string $navigationLabel = 'Komentarze';

    protected static ?string $navigationIcon = 'heroicon-o-chat-alt-2';

    protected static ?int $navigationSort = 3;

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Textarea::make('comment')->label('Komantarz')->columnSpan(['2xl' => 2])->maxLength(65535),
                Select::make('post_id')->label('Wpis')
                    ->relationship('post', 'title')
                    ->preload(),
                Select::make('user_id')->label('Autor')
                    ->relationship('user', 'name')
                    ->preload(),
                TextInput::make('signature')->label('Podpis'),
                TextInput::make('email')->label('Email'),
                Toggle::make('is_approved')->label('Zatwierdzony')
                    ->columnSpan(['2xl' => 2]),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('id')->sortable(),
                TextColumn::make('comment')->size('sm')->wrap()->label('Komentarz'),
                ToggleColumn::make('is_approved')->label('Zatwierdzony'),
                TextColumn::make('post.title')->label('Wpis')->sortable()->searchable(),
                TextColumn::make('signature')->label('Podpis')->sortable()->searchable(),
                TextColumn::make('email')->label('Email')->sortable()->searchable(),
                TextColumn::make('created_at')->label('Utworzony')->sortable(),
            ])
            ->filters([
                TernaryFilter::make('is_approved')->label('Zatwierdzony'),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ])->defaultSort('created_at', 'desc');
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ManageComments::route('/'),
        ];
    }
}
