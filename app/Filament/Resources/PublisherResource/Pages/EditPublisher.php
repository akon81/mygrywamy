<?php

namespace App\Filament\Resources\PublisherResource\Pages;

use Filament\Pages\Actions;
use Illuminate\Support\Facades\Cache;
use Filament\Resources\Pages\EditRecord;
use App\Filament\Resources\PublisherResource;

class EditPublisher extends EditRecord
{
    protected static string $resource = PublisherResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }

    protected function afterSave(): void
    {
        Cache::flush();
    }
}
