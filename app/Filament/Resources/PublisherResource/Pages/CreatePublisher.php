<?php

namespace App\Filament\Resources\PublisherResource\Pages;

use Illuminate\Support\Facades\Cache;
use Filament\Resources\Pages\CreateRecord;
use App\Filament\Resources\PublisherResource;

class CreatePublisher extends CreateRecord
{
    protected static string $resource = PublisherResource::class;

    protected function afterCreate(): void
    {
        Cache::flush();
    }
    
}
