<?php

namespace App\Filament\Resources\ArtistResource\Pages;

use App\Filament\Resources\ArtistResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ManageRecords;

class ManageArtists extends ManageRecords
{
    protected static string $resource = ArtistResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
