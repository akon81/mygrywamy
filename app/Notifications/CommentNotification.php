<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CommentNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $signature;

    public $comment;

    public $post;

    /**
     * Create a new notification instance.
     */
    public function __construct($signature, $comment, $post)
    {
        $this->signature = $signature;
        $this->comment = $comment;
        $this->post = $post;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject('Użytkownik przesłał komentarz')
            ->greeting('Cześć!')
            ->line('Użytkownik: '.$this->signature.' przesłał komentarz. Komentarz dotyczy posta: '.$this->post)
            ->line('Treść komentarza:')
            ->line($this->comment)
            ->salutation(' ');
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
