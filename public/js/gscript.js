var tl = gsap.timeline({repeat: 10, repeatDelay: 1});
tl.to("#back-circle1", {x:450, y: 425, opacity:0.2, ease:"power2.out", duration: 10});
tl.to("#back-circle1", {x: 0, y: 0, opacity:1, ease:"power.in", duration: 8});

var tl2 = gsap.timeline({repeat: 10, repeatDelay: 1});
tl2.to("#back-circle2", {x:700, y: -625, opacity:0.2, ease:"power2.out", duration: 20});
tl2.to("#back-circle2", {x: 0, y: 0, opacity:1, ease:"power.in", duration: 20});