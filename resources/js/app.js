import './bootstrap';

import Alpine from 'alpinejs';
import Tooltip from "@ryangjchandler/alpine-tooltip";
import focus from '@alpinejs/focus';
window.Alpine = Alpine;

Alpine.plugin(focus);
Alpine.plugin(Tooltip);


Alpine.start();
