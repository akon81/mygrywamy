<div class="js-cookie-consent ak-cookie text-center">
    <div class="ak-cookie-ins">
        <img src="{{ asset('img/logo-invert.svg') }}" width="20%">
    <p class="ak-cookie-content">{!! trans('cookie-consent::texts.message') !!}</p>
    <div class="ak-cookie-content-akc">
    <button class="js-cookie-consent-agree cookie-consent__agree cursor-pointer ak-cookie-link-akc">
        {{ trans('cookie-consent::texts.agree') }}
    </button></div>
    </div>
</div>