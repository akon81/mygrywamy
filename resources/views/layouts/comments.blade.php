<div id="comments">
    <h2>Komentarze</h2>

<div class="container">
    <div class="col-md-12">

    <div class="panel">
        <form method="POST" class="w-100 text-center" action="{{ route('comment-send', $post->id) }}">
            @csrf
            <div class="panel-body">
                <div class="mb-2">
                    <input type="text" id="signature" value="@if(Auth::user()){{Auth::user()->name}}@endif" name="signature" class="form-control" placeholder="Podpis" @error('signature') is-invalid @enderror required autocomplete="signature"/>
                    @error('signature')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
        @enderror
                </div>
                <textarea class="form-control input-main-texarea @error('comment') is-invalid @enderror" id="comment" name="comment" rows="2" placeholder="Napisz komentarz..."></textarea>
                @error('comment')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                <div class="pt-4 mb-4 d-flex flex-column align-items-center">
                    <input type="submit" id="submit" class="button-main inv big" value="Opublikuj komentarz">
                </div>
            </div>
        </form>
    </div>

    @foreach($comments as $comment)
        <x-elements.comment author="{{ $comment->signature }}" date="{{ $comment->getFormattedDate() }}">{{ $comment->comment }}</x-elements.comment>
    @endforeach  

    {{ $comments->links() }}
    </div>
    </div>

</div>