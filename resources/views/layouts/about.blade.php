<section id="about" class="container-fluid">
        <div class="row">
            <div id="about-photo" class="col-4 d-none d-lg-block">
                <img src="{{ asset('img/about.jpg') }}" class="about-img">
            </div>

            <div id="about-description" class="col d-flex flex-column justify-content-around p-4">
                <div class="about-header align-self-sm-end align-self-center d-flex flex-row">
                    <h2>O nas</h2>
                    <span><img class="dice-big-inv" src="{{ asset('img/dice_invert.svg') }}"></span>
                </div>

                <div class="px-3 px-sm-5">
                    <h3>Gry planszowe to nasza pasja</h3>
                    <p class="my-4">Przedstawiamy planszówki jako sposób na spędzanie czasu z dziećmi, relaks ze znajomymi i małżeńską rywalizację.</p>
                    <p class="my-4">Znajdziecie u nas zapowiedzi, recenzje gier, relacje z imprez planszówkowych</p>
                </div>

                <div class="align-self-end pe-4">
                    <a href="{{ route('contact')}}" class="button-main inv big">kontakt z nami</a>
                </div>
            </div>
        </div>
    </section>