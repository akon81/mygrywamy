<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
            <!-- Google tag (gtag.js) --> <script async src="https://www.googletagmanager.com/gtag/js?id=G-HFYRFTKS8L"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-HFYRFTKS8L'); </script>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <title>{{ config('app.name') }} @yield('title')</title>
            <meta name="description" content="@yield('description','Gry planszowe, planszówki, recenzje gier planszowych, recenzje gier wydawnictwa Lucky Duck Games')"/>
            <meta property="og:title" content="@yield('title')"/>
            <meta property="og:type" content="website" />
            <meta property="og:description" content="@yield('description','Gry planszowe, planszówki, recenzje gier planszowych, recenzje gier wydawnictwa Lucky Duck Games')"/>
            <meta property="og:locale" content="pl_PL"/>
            <meta property="og:image" content="@yield('og-image', asset('img/logo.svg') )"/>
            <meta property="og:image:width" content="400">
            <meta property="og:image:height" content="400">
            <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('img/favicon/apple-icon-57x57.png') }}">
            <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('img/favicon/apple-icon-60x60.png') }}">
            <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('img/favicon/apple-icon-72x72.png') }}">
            <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('img/favicon/apple-icon-76x76.png') }}">
            <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('img/favicon/apple-icon-114x114.png') }}">
            <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('img/favicon/apple-icon-120x120.png') }}">
            <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('img/favicon/apple-icon-144x144.png') }}">
            <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('img/favicon/apple-icon-152x152.png') }}">
            <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/favicon/apple-icon-180x180.png') }}">
            <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('img/favicon/android-icon-192x192.png') }}">
            <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon/favicon-32x32.png') }}">
            <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('img/favicon/favicon-96x96.png') }}">
            <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon/favicon-16x16.png') }}">
            <link rel="manifest" href="{{ asset('img/favicon/manifest.json') }}">
            <meta name="msapplication-TileColor" content="#ffffff">
            <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
            <meta name="theme-color" content="#ffffff">
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
            <link rel="stylesheet" href="{{ asset('css/style.css') }}">
            <link rel="stylesheet" href="{{ asset('js/owlcarousel/assets/owl.carousel.css') }}">
            <link rel="stylesheet" href="{{ asset('js/owlcarousel/assets/owl.theme.default.min.css') }}">
            @yield('css')
            <!-- Styles -->
            @livewireStyles
    </head>
    <body>

        @yield('animation-section', view('layouts.animation'))

        <header id="header" class="header-section">
            <div id="top" class="container-fluid page-header">
                <div class="row">
                    <div id="head-left" class="col-7 col-lg-3 col-fluid d-flex">
                        <div id="logo">
                            <a href="{{ route('home')}}"><img src="{{ asset('img/logo.svg') }}" class="logo" alt="logo mygrywamy"></a>
                        </div>
                        <div id="sentence"><p><span>Gry planszowe</span> - nasz sposób na spędzanie czasu z dziećmi, relaks ze znajomymi oraz małżeńska rywalizacja.</p></div>
                    </div>
    
                    <div class="col-5 d-sm-block d-lg-none justify-content-between">
                        <span class="d-flex float-end p-2 p-sm-3">
                            <a href="mailto:kontakt@mygrywamy.pl"><img src="{{ asset('img/icon-mail.svg') }}" class="icon icon-soc-min m-sm-1 svg-inv hover-inv"><span class="d-none d-md-inline contact">kontakt@mygrywamy.pl</span></a>
                            <a href="https://facebook.com/mygrywamy"><img src="{{ asset('img/facebook.svg') }}" class="icon icon-soc-min m-sm-1 svg-inv hover-inv"></a> 
                            <a href="https://instagram.com/mygrywamy"><img src="{{ asset('img/instagram.svg') }}" class="icon icon-soc-min m-sm-1 svg-inv hover-inv"></a> 
                        </span>
                    </div>
    
    
                    <div id="head-right" class="col col-sm-6 col-lg-9 d-flex flex-column" style="background-color: rgba(110, 166, 122, 0);">
    
                        <div id="social" class="d-none d-lg-block">
                            <div><a href="mailto:kontakt@mygrywamy.pl"><img src="{{ asset('img/icon-mail.svg') }}" class="icon icon-soc svg hover"><span class="d-none d-md-inline">kontakt@mygrywamy.pl</span></a>
                            <span class="d-none d-lg-inline ms-3">Znajdziesz nas również na: </span>
                                <a href="https://facebook.com/mygrywamy"><img src="{{ asset('img/facebook.svg') }}" class="icon icon-soc svg hover"></a> 
                                <a href="https://instagram.com/mygrywamy"><img src="{{ asset('img/instagram.svg') }}" class="icon icon-soc svg hover"></a> 
                            </div>
                        </div>
    
                        <div id="menu-top" class="bg1">
                            <nav class="navbar navbar-expand-lg navbar-light d-flex float-lg-between">
    
                                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
            
                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <span class="navbar-span d-flex flex-column flex-lg-row justify-content-between">
                                        <ul class="navbar-nav">
                                            <li class="nav-item active not-square">
                                                <a class="nav-link" href="{{ route('home')}}"><img src="{{ asset('img/home.svg') }}" class="icon home svg hover" alt="home"></a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('about') }}">O nas</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('reviews') }}">Recenzje</a>
                                            </li>
               
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('events') }}">Wydarzenia</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('contact')}}">Kontakt</a>
                                            </li>
                                        </ul>
                                            <div class="search">
                                                <form action="{{ route('search') }}" method="GET" class="form-search d-flex">
                                                    <input type="search" name="search" required class="form-control inp-search" placeholder="szukaj...">
                                                    <button type="submit">Search</button>
                                                </form>
                                            </div>
                                     </span>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    
        <main role="main">
                
        @yield('section-main')
    
        @yield('section-tags')
    
        @yield('section-publishings')    
    
        </main>
    
        <footer class="container-fluid">
            <div id="footer" class="row">
                <div class="footer-brand col-lg-4">
                    <img src="{{ asset('img/logo-invert.svg') }}" class="logo" alt="logo mygrywamy" style="display:inline-block;"><h2 style="display:inline;">MyGrywamy</h2>
                </div>
                <div class="footer-menu col-lg-8">
                    <ul class="navbar-nav navbar-footer d-flex flex-column flex-lg-row justify-content-center">
                        <li class="pt-2 pt-lg-4 ps-3 me-3"><a class="navbar-footer-a" href="{{ route('home') }}"><img src="{{ asset('img/home.svg') }}" class="icon icon-soc-min svg-footer hover" alt="home"></a></li>
                        <li class="pt-2 pt-lg-4 ps-3 me-3"><a class="navbar-footer-a" href="{{ route('about') }}">O nas</a></li>
                        <li class="pt-2 pt-lg-4 ps-3 me-3"><a class="navbar-footer-a" href="{{ route('reviews') }}">Recenzje</a></li>
                        <li class="pt-2 pt-lg-4 ps-3 me-3"><a class="navbar-footer-a" href="{{ route('events') }}">Wydarzenia</a></li>
                        <li class="pt-2 pt-lg-4 ps-3 me-3"><a class="navbar-footer-a" href="{{ route('contact')}}">Kontakt</a></li>
                    </ul>
                </div>
     
                <div class="col-12 footer-social align-self-end d-flex flex-column flex-lg-row justify-content-between">
                    <div class="f-social">
                        <a href="https://facebook.com/mygrywamy"><img src="{{ asset('img/facebook.svg') }}" class="icon icon-soc m-sm-1 svg-footer hover"></a> 
                        <a href="https://instagram.com/mygrywamy"><img src="{{ asset('img/instagram.svg') }}" class="icon icon-soc m-sm-1 svg-footer hover"></a> 
                        <a href="mailto:kontakt@mygrywamy.pl"><img src="{{ asset('img/icon-mail.svg') }}" class="icon icon-soc svg-footer hover"><span class="d-none d-md-inline">kontakt@mygrywamy.pl</span></a>
                    </div>
                    <div class="f-created">
                        Realizacja i wdrożenie: Konrad Adamczyk
                    </div>
                </div>
            </div>
        </footer>
       
        <a class="scrollTopButton fi flaticon-arrow-up mb-lg-2" href="#top" title="przejdź do góry"></a>
        @include('cookie-consent::index')
        
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.11.4/gsap.min.js"></script>
    <script src="{{ asset('js/jquery-3.6.4.min.js') }}"></script>
    <script src="{{ asset('js/gscript-main.js') }}"></script>
    <script src="{{ asset('js/owlcarousel/owl.carousel.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.owl-carousel').owlCarousel({
                loop:true,
                margin:10,
                autoWidth:true,
                autoplay:true,
                autoplayTimeout:3000,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:3,
                        nav:true
                    },
                    576:{
                        items:5,
                        nav:false
                    },
                    991:{
                        items:7,
                        nav:false,
                        loop:true
                    }
                }
            });

            $(window).scroll(function() {
			if ($(window).scrollTop() > 400) {
			$('.scrollTopButton').addClass('show');
			} else {
			$('.scrollTopButton').removeClass('show');
			}
		});
		
            $('.scrollTopButton').click(function() {
                $("html,body").animate({scrollTop: 0},{duration: 400, ease: "linear"});
            });

        });
    </script>

    @yield('js-files', view('layouts.js-default'))    

        @livewireScripts
    </body>
</html>
