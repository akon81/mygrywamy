<section id="publishings">
    
    <x-elements.header-main type="blue" header="Wydawnictwa"/>

     <div class="owl-carousel owl-theme">

      @foreach ($publishings as $publisher)
             <x-elements.publishing href="{{ route('publisher', $publisher->slug)}}" logo="{{ $publisher->logo }}" name="{{ $publisher->name }}"/>
      @endforeach

     </div>
</section>