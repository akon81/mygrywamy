<section id="last-added">

    <x-elements.header-main type="blue" header="Ostatnio dodane"/>

    <div class="d-flex justify-content-center flex-wrap">

      @foreach($last_added as $card)
      <x-card-main date="{{ $card->getFormattedDate() }}" classKind="{{ $card->getCategoryMainSlug() }}" kind="{{ $card->getCategoryMainName() }}" 
        title="{{ $card->title }}" img="{{ $card->thumbnail }}" link="{{ route($card->getCategoryMainSlug(), $card->slug) }}">
        {{ $card->getShortTeaser() }}
      </x-card-main>
      @endforeach

    </div>

</section>