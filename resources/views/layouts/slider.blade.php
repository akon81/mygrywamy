<section id="slider-main" class="container-fluid">
        
        <div class="row">
            <div id="slider" class="col col-fluid">
                    <div id="carouselExampleCaptions" class="carousel slide carousel-fade" data-bs-ride="carousel">
                        <div class="carousel-indicators">
                            @for ($i = 0; $i < $slider->count(); $i++)
                                    @if ($i==0)
                                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="{{ $i }}" class="active d-none d-sm-block" aria-current="true" aria-label="slajd {{ $i }}"></button>
                                    @endif
                                    @if ($i>0)
                                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="{{ $i }}" class="d-none d-sm-block" aria-label="slajd {{ $i }}"></button>
                                    @endif
                             @endfor
                        </div>
                        <div class="carousel-inner d-flex align-items-center">
                            
                            @foreach($slider as $slajd)
                            @if($slajd->getMedia('gallery')->first()!=null)
                            <div class="carousel-item @if ($loop->first) active @endif">
                                <img src="{{ asset($slajd->getMedia('gallery')->first()->getUrl()) }}" class="d-block w-100" alt="{{ $slajd->title}}">
                                <div class="carousel-caption" id="carousel-first-caption">
                                <h5 class="mt-lg-3">{{ $slajd->title}}</h5>
                                <p>{{ $slajd->getShortTeaser(15) }}</p>
                                <span class="fixed-bottom pb-2"><a href="{{ route($slajd->getCategoryMainSlug(), $slajd->slug) }}" class="button-main m-lg-2">zobacz więcej</a>
                                </span></div>
                            </div>
                            @endif
                            @endforeach
                            
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Poprzedni</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Następny</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>

    </section>