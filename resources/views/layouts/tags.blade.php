<section id="tags">

    <x-elements.header-main type="purple" header="Tagi"/>

     <div class="tags-main d-flex flex-row justify-content-center flex-wrap">

      @foreach ($tags as $tag)
             <x-elements.tag href="{{ route('tag', $tag->slug)}}" title="{{ $tag->tag }}"/>
      @endforeach

     </div>

</section>