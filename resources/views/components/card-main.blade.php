<div class="card">
    <div class="card-head d-flex flex-row">
        <div class="card-date">{{ $date }}</div>
        <div class="card-kind {{ $classKind }}">{{ $kind }}</div>
    </div>
    <div class="card-body text-center">
      <h5 class="card-title">{{ $title }}</h5>
      <p class="card-text">{{ $slot }}</p>
      <div class="card-img-div"><a href="{{ $link }}"><img src="{{ asset('storage/'.$img) }}" class="card-img" alt="{{ $title }}"></a></div>
      <a href="{{ $link }}" class="button-main mt-3">zobacz więcej</a>
    </div>
</div>