@props(['header', 'slot', 'descript' => 'false'])

<div class="header-main header-background d-flex flex-row justify-content-center align-items-center">
    <h2>{{ $header }}</h2>
</div>
        
    @if ($descript=='true')
        <h4 class="pt-3 text-center">{{ $slot }}</h4>
    @endif