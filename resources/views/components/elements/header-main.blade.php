@props(['type' => 'purple', 'header'])

<div {{ $attributes->merge(['class' => 'header-main d-flex flex-row justify-content-center header-'.$type]) }}>
    <h2>{{ $header }}</h2>
</div>