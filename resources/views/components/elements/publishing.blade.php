@props(['href', 'logo', 'name'])

<div class="item"><a href="{{ $href }}"><img class="carousel-img" src="{{ asset('storage/'.$logo) }}" alt="{{ $name }}"></a></div>