@props(['href', 'title'])

<div class="tag"><a href="{{ $href }}">{{ $title }}</a></div>