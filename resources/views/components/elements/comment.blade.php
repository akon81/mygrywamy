@props(['author', 'date'])

<div class="panel">
    <div class="panel-body">
        <div class="media-body">
        <p class="p-0 m-0">{{ $author }}</p>
        <p class="text-muted text-sm"><i class="fa fa-claendar fa-lg"></i>{{ $date }}</p>
        <p>{{ $slot }}</p>                
        </div>
    <hr/>
    </div>
</div>