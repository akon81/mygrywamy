@extends('layouts.app')

@section('title')
- {{ $page->meta_title }}
@endsection

@section('description')
{{ $page->meta_description }}
@endsection

@section('section-main')
       
        @include('layouts.about')
    
        <div id="about-description-more" class="col-12 d-flex flex-column justify-content-around p-4">
            {!! $page->content !!}
        </div>

@endsection

@section('section-tags')
    @include('layouts.tags')
@endsection

@section('section-publishings')
    @include('layouts.publishings')
@endsection