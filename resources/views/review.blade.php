@extends('layouts.app')

@section('title') {{ $post->meta_title }}@endsection

@section('description'){{ $post->meta_description }}@endsection

@section('og-image'){{ asset('storage/'.$post->thumbnail) }}@endsection

@section('css')
<link href="{{ asset('vendor/lightbox/css/lightbox.css') }}" rel="stylesheet" />
@endsection

@section('animation-section')
        @empty($post->animation)
            <div class="animation-ob" id="back-circle1"></div>
            <div class="animation-ob" id="back-circle2"></div>
        @endempty
        @isset($post->animation)
            <div id="details_object"><img src="{{ asset('storage/'.$post->spirit) }}"></div>
        @endisset
@endsection

@section('section-main')

@if (session('status'))
<div class="alert alert-primary">
    {{ session('status') }}
</div>
@endif

<section id="review" class="container-fluid">

    <div class="row">
        <div id="main-photo" class="col-md-4">
            <img src="{{ asset('storage/'.$post->thumbnail) }}" class="img-thumb">
        </div>

        <div id="details-description" class="col-md d-flex flex-column p-4">
            <div class="px-3 px-sm-5 pt-2">
                <h1>{{ $post->title }}</h1>
                <ul>
                    <li>Autor: <span>{{ $post->authors->implode('name', ', ') }}</span></li>
                    <li>Ilustrator: <span>{{ $post->artists->implode('name', ', ') }}</span></li>
                    @isset($post->publisher)
                    <li>Wydawnictwo: <span><a class="text-white" href="{{ route('publisher', $post->publisher->slug)}}">{{ $post->publisher->name  }}</a></span></li>
                    @endisset
                    <li>Liczba graczy: <span>{{ $post->players }} graczy</span></li>
                    <li>Czas gry: <span>{{ $post->time }} minut</span></li>
                    <li>Wiek: <span>{{ $post->age }} +</span></li>
                </ul>
            </div>
        </div>

        <div id="details-more" class="row d-flex flex-row justify-content-left p-4">
            <div class="details-more-div col-12">
                <img src="{{ asset('img/tag.svg') }}">
                <span style="margin-left:10px; margin-top:8px;">@foreach ($post->tags as $tag) <a href="{{ route('tag', $tag->slug)}}">{{ $tag->tag }} |</a> @endforeach</span>
            </div>
            <div class="details-more-div col-12">
                <img src="{{ asset('img/clock.svg') }}">
                <span style="margin-left:10px; margin-top:8px;">Przeczytasz w {{ $post->humanReadTime() }} min.</span>
            </div>
            <div class="details-more-div col-12">
                <img src="{{ asset('img/calendar.svg') }}">
                <span style="margin-left:10px; margin-top:8px;">{{ $post->getFormattedDate() }}</span>
            </div>
        </div>

        <div id="about-description-more" class="col-12 d-flex flex-column justify-content-around p-4">
           
            {!! $post->content !!}

            <div class="d-flex flex-row justify-content-center">
                <p class="rating">Ocena: <span>{{ $post->rating }}/10</span></p>
            </div>
           
            <div id="gallery">
                @foreach($post->getMedia('gallery') as $gallery)
                    @if ($loop->first)
                        <h2>Galeria</h2>
                        <div class="d-flex flex-lg-row flex-column flex-wrap justify-content-around">
                    @endif
                        <div class="p-2">
                            <a href="{{ asset($gallery->getUrl()) }}" data-lightbox="gallery" data-title="{{ $gallery->name }}">
                                <img class="img-fluid" src="{{ asset($gallery->getUrl('thumb')) }}">
                            </a>
                        </div>

                    @if ($loop->last)
                        </div>
                    @endif
                @endforeach
            </div>

        </div>

@include('layouts.comments')

    </div>

</section>     

@endsection

@section('section-tags')
    @include('layouts.tags')
@endsection

@section('section-publishings')
    @include('layouts.publishings')
@endsection

@section('js-files')
    @empty($post->animation)
        <script src="{{ asset('js/gscript.js') }}"></script>
    @endempty
    @isset($post->animation)
        <script src="{{ asset('storage/'.$post->animation) }}"></script>
    @endisset
<script src="{{ asset('vendor/lightbox/js/lightbox.js') }}"></script>
@endsection