@extends('layouts.app')

@section('title')
- tag: {{ $tag->tag }}
@endsection

@section('section-main')
<section id="content-list">
  
    <x-elements.header-top header="Tag" descript="true">{{ $tag->tag }}</x-elements.header-top>

    <div class="p-1 d-flex flex-row justify-content-center flex-wrap"><p>{!! $tag->description !!}</p></div>

    <div class="d-flex justify-content-center flex-wrap pt-5">

        @forelse($posts as $card)
      <x-card-main date="{{ $card->getFormattedDate() }}" classKind="{{ $card->getCategoryMainSlug() }}" kind="{{ $card->getCategoryMainName() }}" 
        title="{{ $card->title }}" img="{{ $card->thumbnail }}" link="{{ route($card->getCategoryMainSlug(), $card->slug) }}">
        {{ $card->getShortTeaser() }}
      </x-card-main>
      @empty
        <h3>Brak wpisów</h3>
      @endforelse
     
    </div>

    <div class="paginate-main p-3 d-flex flex-row justify-content-center flex-wrap">
        {{ $posts->links() }}
    </div>
   
</section>      

@endsection

@section('section-tags')
    @include('layouts.tags')
@endsection

@section('section-publishings')
    @include('layouts.publishings')
@endsection