@extends('layouts.app')

@section('title')
- {{ $publisher->name }}
@endsection

@section('section-main')
<section id="content-list">
  
    <x-elements.header-top header="Wydawnictwo" descript="true">{{ $publisher->name }}</x-elements.header-top>

    <div class="publishing-head p-1 d-flex flex-row justify-content-center flex-wrap"><p><a href="{{ $publisher->www }}"><img src="{{ asset('storage/'.$publisher->logo) }}" class="card-img" alt="{{ $publisher->www }}"></a></p></div>
    <div class="publishing-head mx-4 px-4 p-1 d-flex justify-content-center"><div class="publishing-desc">{!! $publisher->description !!}</div></div>

    <div class="d-flex justify-content-center flex-wrap">

        @forelse($posts as $card)
      <x-card-main date="{{ $card->getFormattedDate() }}" classKind="{{ $card->getCategoryMainSlug() }}" kind="{{ $card->getCategoryMainName() }}" 
        title="{{ $card->title }}" img="{{ $card->thumbnail }}" link="{{ route($card->getCategoryMainSlug(), $card->slug) }}">
        {{ $card->getShortTeaser() }}
      </x-card-main>
      @empty
        <h3>Brak recenzji wybranego wydawnictwa</h3>
      @endforelse
     
    </div>

    <div class="paginate-main p-3 d-flex flex-row justify-content-center flex-wrap">
        {{ $posts->links() }}
    </div>
   
</section>

      

@endsection

@section('section-tags')
    @include('layouts.tags')
@endsection

@section('section-publishings')
    @include('layouts.publishings')
@endsection