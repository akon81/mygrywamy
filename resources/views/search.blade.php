@extends('layouts.app')

@section('title')
- wyniki wyszukiwania
@endsection

@section('section-main')
<section id="content-list">
  
    <x-elements.header-top header="Wyniki wyszukiwania" descript="false"></x-elements.header-top>

    <div class="d-flex justify-content-center flex-wrap pt-5">

        @forelse ($posts as $card)
        <x-card-main date="{{ $card->getFormattedDate() }}" classKind="{{ $card->getCategoryMainSlug() }}" kind="{{ $card->getCategoryMainName() }}" 
            title="{{ $card->title }}" img="{{ $card->thumbnail }}" link="{{ route($card->getCategoryMainSlug(), $card->slug) }}">
            {{ $card->getShortTeaser() }}
          </x-card-main>
            @empty
                <h3>Brak wyników wyszukiwania</h3>
            @endforelse

      
    </div>

    <div class="paginate-main p-3 d-flex flex-row justify-content-center flex-wrap">
        {{ $posts->links() }}
    </div>
   
</section>  

@endsection

@section('section-tags')
    @include('layouts.tags')
@endsection

@section('section-publishings')
    @include('layouts.publishings')
@endsection