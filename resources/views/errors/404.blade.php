@extends('layouts.app')

@section('title')
- 404 
@endsection

@section('section-main')
    <div id="center-section" class="col-lg-12 col-sm-12">
        <div class="container mt-5 pt-5" style="min-height: 550px;">
            <div class="alert text-center">
                <h2 class="display-3">404</h2>
                <p class="display-5 pt-4 pb-4">Ups! Coś poszło nie tak.</p>
                <p class="display-6 pt-3"><a href="{{ route('home') }}">Przejdź do strony głównej</a></p>
            </div>
        </div>
    </div>      
    
@endsection