@extends('layouts.app')

@section('title')
- Kontakt
@endsection

@section('section-main')
<section id="contact">
  
    <x-elements.header-top header="Kontakt" descript="true">Jeśli masz dla nas propozycję współpracy skontaktuj się z nami</x-elements.header-top>

    @if (session('status'))
    <div class="alert alert-primary">
        {{ session('status') }}
    </div>
    @endif

       <div class="p-3 d-flex flex-row justify-content-center">
            
        <form method="POST" class="w-100 text-center" action="{{ route('contact-send') }}">
            @csrf

                <div class="mb-2 d-flex flex-column align-items-center">
                    <label for="email">Adres email</label>
                    <input type="email" id="email" name="email" class="form-control input-main @error('email') is-invalid @enderror" placeholder="adres email" value="{{ old('email') }}" required autocomplete="email"/>
                    @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                </div>

                <div class="mb-2 d-flex flex-column align-items-center">
                    <label for="content">Treść wiadomości</label>
                    <textarea id="content" name="content" class="form-control input-main input-main-texarea @error('content') is-invalid @enderror" required></textarea>
                    @error('content')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                </div>

                <div class="pt-4 mb-4 d-flex flex-column align-items-center">
                    <input type="submit" id="submit" class="button-main inv big" value="WYŚLIJ">
                </div>

            </form>
        </div>

</section>
@endsection

@section('section-tags')
    @include('layouts.tags')
@endsection

@section('section-publishings')
    @include('layouts.publishings')
@endsection