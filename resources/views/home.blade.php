@extends('layouts.app')

@section('title')
- Strona główna 
@endsection

@section('section-main')
        @include('layouts.slider')

        @include('layouts.about')

        @include('layouts.last_added')
    
@endsection

@section('section-tags')
    @include('layouts.tags')
@endsection

@section('section-publishings')
    @include('layouts.publishings')
@endsection