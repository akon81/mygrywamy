<?php

use App\Models\Publisher;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('title', 255);
            $table->string('slug')->unique();
            $table->text('teaser')->nullable();
            $table->longText('content')->nullable();
            $table->string('thumbnail')->nullable();
            $table->string('animation')->nullable();
            $table->string('spirit')->nullable();
            $table->boolean('is_active')->default(false);
            $table->boolean('in_slider')->default(false);
            $table->datetime('published_at')->nullable();
            $table->string('players', 255)->nullable();
            $table->string('age', 255)->nullable();
            $table->string('time', 255)->nullable();
            $table->string('rating', 255)->nullable();
            $table->string('meta_title', 255)->nullable();
            $table->string('meta_description', 255)->nullable();
            $table->foreignIdFor(User::class, 'user_id');
            $table->foreignIdFor(Publisher::class, 'publisher_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('posts');
    }
};
