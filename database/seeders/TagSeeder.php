<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('tags')->insert([
            'tag' => 'familijne',
            'slug' => 'familijne',
        ]);

        DB::table('tags')->insert([
            'tag' => 'dwuosobówki',
            'slug' => 'dwuosobowki',
        ]);

        DB::table('tags')->insert([
            'tag' => 'karcianki',
            'slug' => 'karcianki',
        ]);
    }
}
