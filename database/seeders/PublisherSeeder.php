<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PublisherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('publishers')->insert([
            'name' => 'Lucky Duck Games',
            'slug' => 'lucky-duck-games',
            'www' => 'https://luckyduckgames.com/pl/',
            'logo' => 'publisher/luckyduck.png',
        ]);
    }
}
