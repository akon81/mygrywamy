<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('pages')->insert([
            'title' => 'O nas',
            'slug' => 'o-nas',
            'content' => '',
            'in_menu_top' => 1,
            'in_menu_bottom' => 1,
            'is_active' => 1,
            'order' => 1,
            'meta_title' => 'O nas',
            'meta_description' => 'Przedstawiamy planszówki jako sposób na spędzanie czasu z dziećmi, relaks ze znajomymi i małżeńską rywalizację',
        ]);
    }
}
