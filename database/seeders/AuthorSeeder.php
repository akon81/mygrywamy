<?php

namespace Database\Seeders;

//use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('authors')->insert([
            'name' => 'Uwe Rosenberg',
            'description' => 'Born 27 March 1970 in Aurich, Germany',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
