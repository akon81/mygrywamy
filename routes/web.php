<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ContentController;
use App\Http\Controllers\SitemapController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'index']);
Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/kontakt', [HomeController::class, 'contact'])->name('contact');
Route::post('/kontakt/wyslij', [HomeController::class, 'send'])->name('contact-send');
Route::get('/strona/o-nas', [HomeController::class, 'about'])->name('about');

Route::post('/komentarz/wyslij/{post:id}', [ContentController::class, 'comment_send'])->name('comment-send');

Route::get('/tag/{tag:slug}', [ContentController::class, 'tag_index'])->name('tag');

Route::get('/wydawnictwo/{publisher:slug}', [ContentController::class, 'publisher_index'])->name('publisher');

Route::get('/recenzje', [ContentController::class, 'reviews_index'])->name('reviews');
Route::get('/wydarzenia', [ContentController::class, 'events_index'])->name('events');
Route::get('/zapowiedzi', [ContentController::class, 'announcements_index'])->name('announcements');

Route::get('/recenzja/{post:slug}', [ContentController::class, 'show_review'])->name('recenzja');
Route::get('/wydarzenie/{post:slug}', [ContentController::class, 'show_review'])->name('wydarzenie');
Route::get('/zapowiedz/{post:slug}', [ContentController::class, 'show_review'])->name('zapowiedz');

Route::get('/wyszukiwanie', [ContentController::class, 'search'])->name('search');

Route::get('/sitemap', [SitemapController::class, 'makeSitemap']);
