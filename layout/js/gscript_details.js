var tl = gsap.timeline({repeat: 10, repeatDelay: 1});
tl.to("#details_object", {x: () => innerWidth+100, y: () => innerHeight, scale:0.4, opacity:0.3, ease:"power2.out", duration: 15});
tl.to("#details_object", {x: () => innerWidth/3, y: () => innerHeight/2, scale:1, opacity:1, ease:"power.in", duration: 8});
tl.to("#details_object", {x: 0, y: () => innerHeight+100, scale:0.3, opacity:0.1, ease:"power.out", duration: 6});
tl.to("#details_object", {x: () => innerWidth/2, y: () => innerHeight/5, scale:1, opacity:1, ease:"power.in", duration: 8});
tl.to("#details_object", {x: 0, y: 0, opacity:1, ease:"power.in", duration: 8});