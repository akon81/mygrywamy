
# MyGrywamy

Blog z recenzjami, zapowiedziami i wydarzeniami związanymi z grami planszowymi


![Logo](https://mygrywamy.pl/img/logo.svg)


## Opis

- Strona: kontakt z formularzem kontaktowym 
- Strona: O nas
- Edycja wszystkich treści z poziomu panelu admina
- Możliwość dodawania recenzji gier planszowych
- Opis dodawany za pomocą edytora WYSIWYG
- Dodawanie miniaturki i zdjęć do galerii
- Dodawanie oceny
- Dodawanie danych typu autor, wydawnictwo, ilustratorzy, tagi itp
- Dodawanie recenzji do slidera na stronie głównej
- Możliwość dodawnia komentarzy do recenzji
- Wyszukiwarka treści
- Wyszukiwanie po tagach, wydawnictwach itp.
## Link do strony

https://mygrywamy.pl/


## Wykorzystane technologie

**Client:** HTML, CSS, Bootstrap5, JQuery, BLADE (laravel), GSAP3.11 (animacje)

**Server:** PHP 8.1, Laravel 10.10, Livewire 2.12, Mysql

**Admin Panel:** Filament 2

## Feedback

Jeśli masz jakieś uwagi, skontaktuj się ze mną pod adresem k.adamczyk81@gmail.com

